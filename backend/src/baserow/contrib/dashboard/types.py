from typing import TypedDict


class DashboardDict(TypedDict):
    id: int
    name: str
    order: int
    type: str
